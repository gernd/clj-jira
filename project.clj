(defproject manenko/clj-jira "0.1.14-SNAPSHOT"
  :description  "A Clojure wrapper around Jira REST API."
  :url          "https://gitlab.com/manenko/clj-jira"
  :scm          {:name "git"
                 :url  "https://gitlab.com/manenko/clj-jira"}
  :license      {:name "EPL-2.0"
                 :url  "https://www.eclipse.org/legal/epl-2.0/"}
  :plugins      [[lein-codox          "0.10.7"]]
  :dependencies [[org.clojure/clojure "1.10.1" :scope "provided"]]
  :repl-options {:init-ns manenko.clj-jira.core}
  :codox        {:metadata {:doc/format :markdown}}
  :profiles     {:dev {:dependencies [[clj-http "3.10.0"]]}})
