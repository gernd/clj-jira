(ns manenko.clj-jira.request.util
  (:require
   [clojure.string :as str]))


(defn make-comma-separated-string
  "Transforms the given sequence of keywords, strings, or symbols into a comma
  delimited string."
  [xs]
  (str/join "," (map name xs)))


(defn opts->query-params
  "Jira REST API supports array query parameters which are represented as a
  comma-delimited string.  This function extracts the given query parameters
  from the given map `m` and transforms them into Jira REST API format.

  **Arguments:**

  opts
  : A map that configures a Jira query and could consist of query parameters.
    The names of query parameters are `(keys k->f)`.

  k->f
  : A map of keywords to functions of one argument.  A keyword represents a name
    of a query parameter.  A function transforms a query parameter value.  It
    could be `nil`, in this case the value is not transformed.

  You typically don't need to use this function unless you want to create your
  own wrappers/builders for Jira REST API requests.  Please, refer to the source
  code of [manenko.clj-jira.request.user/current] function, for an example of
  usage."
  [opts k->f]
  (->>
    (select-keys opts (keys k->f))
    (map (fn [[k v]]
           (if-let [f (k k->f)]
             [k (f v)]
             [k v])))
    (into {})))


(def opts->body-params opts->query-params)
