(ns manenko.clj-jira.http)


(defprotocol AHttpClient
  (-send-request   [this m])
  (-parse-body     [this m])
  (-transform-body [this m]))


(defn send-request
  [client m]
  (->> m
       (-transform-body client)
       (-send-request   client)
       (-parse-body     client)))
