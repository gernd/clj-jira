(ns manenko.clj-jira.core
  "Functions that wrap Jira API.

  ### Design

  Every Jira function in this namespace follows the same pattern and
  accepts at least the following three parameters:

  * `send-fn`. A function of one argument, that accepts a [Ring-style]
    *request* map, sends it to a Jira HTTP server and returns
    a [Ring-style] *response* map.
  * `receive-fn`. A function of one argument, that accepts a [Ring-style]
    *response* map, transforms it and returns the *transformed* value.
  * `opts`. A map (it is optional for some of the functions) that configures
    a Jira query.

  The library doesn't send HTTP requests itself, delegating this job
  to a caller via `send-fn` and `receive-fn` parameters instead.  Each
  function will invoke `send-fn` argument with a request map it
  generated, then return a result of applying `receive-fn` to the
  return value from `send-fn`:

  ```clojure
  (defn typical-function
    [send-fn receive-fn opts]
    (let [request (generate-request-map opts)]
      (-> request
          send-fn
          receive-fn)))
  ```

  This approach allows for zero dependencies library but it comes with
  a price - users have to add a glue between HTTP library of their
  choice and `manenko/clj-jira`.

  ### Quick Start

  This section explains how to use `manenko/clj-jira` and [clj-http]
  libraries together.

  First, require both libraries in your application:

  ```clojure
  (ns manenko.clj-jira.example
    (:require [clj-http.client             :as client]
              [manenko.clj-jira.core       :as jira]
              [manenko.clj-jira.middleware :as middleware]))
  ```

  Then, define a host, email, and [API token] for Jira communication:

  ```clojure
  (def host  \"example.atlassian.net\")
  (def email \"user@example.com\")
  (def token \"DN21KLJh298haishu8AUHIU3\")
  ```

  You can, of course, retrieve them from other sources instead of hardcoding.

  Next step is to define a function that sends an HTTP request and
  integrate it with Jira middleware provided by `manenko/clj-jira`:

  ```clojure
  (defn request
    [m]
    (client/with-middleware
      (conj 
       client/default-middleware
       (middleware/wrap-api        host)
       (middleware/wrap-token-auth email token))
      (client/request m)))
  ```

  Now you can use `request` function to make API calls:
  
  ```clojure
  (jira/user-current-get request :body)
  ```

  [clj-http]: https://github.com/dakrone/clj-http
  [Ring-style]: https://github.com/ring-clojure/ring/blob/master/SPEC
  [API token]: https://confluence.atlassian.com/cloud/api-tokens-938839638.html"
  (:require
   [manenko.clj-jira.http         :as http]
   [manenko.clj-jira.request.core :as jr]))

;; User
;; -----------------------------------------------------------------------------


(defn user-current-get
  ([client]
   (user-current-get client {}))
  ([client opts]
   (http/send-request client (jr/user-current-get opts))))


;; Project
;; -----------------------------------------------------------------------------


(defn project-get
  "Returns a project using the given ID/key.
 
  **Arguments:**

  client
  : An object that implements [[manenko.clj-jira.http/AHttpClient]] protocol.

  id-or-key
  : Project ID or key (case sensitive).

  opts
  : An optional map that configures a Jira query.  Refer to
    [[manenko.clj-jira.request.core/project-get]] function documentation
    for the detailed description of the map."
  ([client id-or-key opts]
   (http/send-request client (jr/project-get id-or-key opts)))
  ([client id-or-key]
   (project-get client id-or-key {})))


(defn project-search
  "Searches for projects visible to the current user.
 
  **Arguments:**

  client
  : An object that implements [[manenko.clj-jira.http/AHttpClient]] protocol.

  opts
  : An optional map that configures a Jira query.  Refer to
    [[manenko.clj-jira.request.core/project-search]] function documentation
    for the detailed description of the map."
  ([client opts]
   (http/send-request client (jr/project-search opts)))
  ([client]
   (project-search client {})))


;; Issue
;; -----------------------------------------------------------------------------


(defn issue-get
  "Returns an issue using the given ID/key.
 
  **Arguments:**

  client
  : An object that implements [[manenko.clj-jira.http/AHttpClient]] protocol.

  id-or-key
  : Issue ID or key.

  opts
  : An optional map that configures a Jira query.  Refer to
    [[manenko.clj-jira.request.core/issue-get]] function documentation
    for the detailed description of the map."
  ([client id-or-key opts]
   (http/send-request client (jr/issue-get id-or-key opts)))
  ([client id-or-key]
   (issue-get client id-or-key {})))


(defn issue-search
  ([client jql opts]
   (http/send-request client (jr/issue-search jql opts)))
  ([client jql]
   (issue-search client jql {})))

