# manenko/clj-jira

[![Clojars Project](https://img.shields.io/clojars/v/manenko/clj-jira.svg)](https://clojars.org/manenko/clj-jira)

A Clojure wrapper around Jira REST API.

## Usage

FIXME

### API Docs and Articles

[![cljdoc badge](https://cljdoc.org/badge/manenko/clj-jira)](https://cljdoc.org/d/manenko/clj-jira/CURRENT)

### Quickstart

This section explains how to use `manenko/clj-jira` and [clj-http]
libraries together.

First, require both libraries in your application:

```clojure
(ns manenko.clj-jira.example
  (:require [clj-http.client             :as client]
            [manenko.clj-jira.core       :as jira]
            [manenko.clj-jira.middleware :as middleware]))
```

Then, define a host, email, and [API token] for Jira communication:

```clojure
(def host  "example.atlassian.net")
(def email "user@example.com")
(def token "DN21KLJh298haishu8AUHIU3")
```

You can, of course, retrieve them from other sources instead of hardcoding.

Next step is to define a function that sends an HTTP request and
integrate it with Jira middleware provided by `manenko/clj-jira`:

```clojure
(defn request
  [m]
  (client/with-middleware
    (conj 
     client/default-middleware
     (middleware/wrap-api        host)
     (middleware/wrap-token-auth email token))
    (client/request m)))
```

Now you can use `request` function to make API calls:

```clojure
(jira/get-current-user request :body)
```

[clj-http]:   https://github.com/dakrone/clj-http
[Ring-style]: https://github.com/ring-clojure/ring/blob/master/SPEC
[API token]:  https://confluence.atlassian.com/cloud/api-tokens-938839638.html


## License

Copyright (c) 2019 Oleksandr Manenko

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
